<?php
use App\Api\Classes\RpcServer;

Route::post('/api/v1/rpc/banks/', function () {
    // Инициализация сервера RpcServer(<КЛАСС СЕРВИСА>)
    $RpcServer = new RpcServer('App\Api\Classes\RpcBanks');
    // Отработка запроса и ответ
    return Response::json($RpcServer->catchRequest());
});