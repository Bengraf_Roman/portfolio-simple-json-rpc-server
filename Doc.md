
### Json-rpc 2.0
[JSON-RPC 2.0 Specification](http://www.jsonrpc.org/specification)
#### Reference
- [Banks](#Banks)
# Service `Banks` <a id="Banks"></a>
#### Main:

| Property | Value |
| :---: | :---: |
| Over | https |
| Host | `/api/v1/rpc/banks` |
| Cache livetime | 24h |

#### Methods:
| Method | Params | Description |
| --- | --- | --- |
| `bankByBic` | *string* **bic** [ min 6, recomended 9] | Поиск банка по БИК |
| `bankByBics` | *string [..]* **bic** [ min 6, recomended 9 ] | Поиск банков по списку БИК|

#### Templates:
##### Success
###### Request
```
{
    "jsonrpc":"2.0",
    "method":"bankByBic",
    "id":"1",
    "params":{
        "bic":"040702752"
    }
}
```
###### Response Success
```
{
    "jsonrpc": "2.0",
    "result":[
        {
            "name": "ФИЛИАЛ \"СТАВРОПОЛЬСКИЙ\" АО \"АЛЬФА-БАНК\"",
            "correspondent": "30101810000000000752"
        }
    ],
    "id": "1"
}
```
##### Response Error
###### Request
```
{
    "jsonrpc":"1.0", //Error: Must be "2.0"
    "method":"bankByBic",
    "id":"1",
    "params":{
        "bic":"040702752"
    }
}

```
###### Response Error
```
{
    "jsonrpc": "2.0",
    "error":{
        "code": -32600,
        "message": "The JSON sent is not a valid Request object.",
        "data": "JsonRpc parameter is invalid"
    },
    "id": "1"
}
```
##### Batch request
###### Request
```
[
    {
        "jsonrpc":"1.0", //Error: Must be "2.0"
        "method":"bankByBic",
        "id":"1",
        "params":{
            "bic":"040702752"
        }
    },
    {
        "jsonrpc":"2.0",
        "method":"bankByBic", //First method
        "id":"2",
        "params":{
            "bic":"040702752"
        }
    },
    {
        "jsonrpc":"2.0",
        "method": "bankByBics", // Other method
        "id":"3",
        "params":{
            "bic":
            [
                "040702752",
                "044030786"
            ]
        }
    },
...
]
```
###### Response Batch
```
[
    {
        "jsonrpc": "2.0",
        "error":{
            "code": -32600,
            "message": "The JSON sent is not a valid Request object.",
            "data": "JsonRpc parameter is invalid"
        },
        "id": "1"
    },
    {
        "jsonrpc": "2.0",
        "result":[
            {
                "name": "ФИЛИАЛ \"СТАВРОПОЛЬСКИЙ\" АО \"АЛЬФА-БАНК\"",
                "correspondent": "30101810000000000752"
            }
        ],
        "id": "2"
    },
    {
        "jsonrpc": "2.0",
        "result":{
            "040702752":[
                {
                    "name": "ФИЛИАЛ \"СТАВРОПОЛЬСКИЙ\" АО \"АЛЬФА-БАНК\"",
                    "correspondent": "30101810000000000752"
                }
            ],
            "044030786":[
                {
                    "name": "ФИЛИАЛ \"САНКТ-ПЕТЕРБУРГСКИЙ\" АО \"АЛЬФА-БАНК\"",
                    "correspondent": "30101810600000000786"
                }
            ]
        },
        "id": "3"
    }
]
```
___