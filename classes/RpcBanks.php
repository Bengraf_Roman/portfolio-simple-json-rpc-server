<?php
/**
 *  / RpcMethods.php
 *
 * Company: app.com
 * User: Roman Bengraf <>
 * Date: 19.01.2017
 * Time: 16:48
 */
namespace App\Api\Classes;

use Cache;
use App\Banks\Models\Bank;

class RpcBanks extends RpcServer
{
    protected static function getCacheKey($method, $strParam)
    {
        return 'JsonRpc|' . $method . '|' . $strParam;
    }

    protected static function bankByBic($params)
    {
        if (!$params) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "Method require 'bic' parameters"
            );
        }
        if (!isset($params['bic']) || !$params['bic']) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "'bic' is required"
            );
        }
        if (!is_string($params['bic']) || empty($params['bic'])) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "'bic' must be a valid string"
            );
        }
        if (strlen($params['bic']) <= 5) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "The string 'bic' is too short. Must be 6 minimum. Recomended 9"
            );
        }
        $cacheKey = 'JsonRpc|' . __METHOD__ . '|' . $params['bic'];
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $result = Bank::select('pay_name as name', 'correspondent_new as correspondent')
                ->where('bik', 'LIKE', $params['bic'] . '%')
                ->get() ?? null;
        Cache::add($cacheKey, $result->toArray(), $minutes = 1440);
        return $result;
    }

    protected static function bankByBics($params)
    {
        if (!$params) {
            parent::$service->setError(
                parent::ERR_INVALID_REQUEST,
                "Method require valid parameters"
            );
            return parent::$service->error;
        }
        $result = [];
        if (!isset($params['bic']) || !$params['bic']) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "'bic' is required"
            );
            return parent::$service->error;
        }
        if (!is_array($params['bic']) || !count($params['bic'])) {
            parent::$service->setError(
                parent::ERR_INVALID_PARAMS,
                "'bic' must be a valid array"
            );

            return parent::$service->error;
        }
        foreach ($params['bic'] as $bic) {
            $result[$bic] = self::bankByBic(['bic' => $bic]);
        }

        return $result;
    }
}
