<?php
/**
 * / RpcServer.php
 **
 * Company: app.com
 * User: Roman Bengraf <>
 * Date: 19.01.2017
 * Time: 11:54
 */
namespace App\Api\Classes;

use Exception;

class RpcServer
{
    /**
     * Коды ошибок
     * http://www.jsonrpc.org/specification#error_object
     */
    const ERR_INVALID_JSON = -32700; // Parse error Invalid JSON was received by the server.
    const ERR_INVALID_REQUEST = -32600; // The JSON sent is not a valid Request object.
    const ERR_METHOD_NOT_FOUND = -32601; // The method does not exist / is not available.
    const ERR_INVALID_PARAMS = -32602; // Invalid method parameter(s).
    const ERR_INTERNAL = -32603; // Internal JSON-RPC error.
    const ERR_SERVER_500 = -32000; // 500
    protected static $service;
    protected $callableClass;
    protected $request;
    protected $result;
    protected $error;
    /**
     * @property string Обязательное поле в запросе с ID запроса
     */
    protected $id;
    /**
     * @property string Обязательное поле в запросе с номером версии JSON-RPC
     */
    protected $jsonrpc;
    /**
     * @property string Обязательное поле в запросе метод
     */
    protected $method;
    /**
     * @property string Обязательное поле в запросе с параметрами дял метода
     */
    protected $params;
    protected static $errors = [
        -32700 => 'Parse error Invalid JSON was received by the server.',
        -32600 => 'The JSON sent is not a valid Request object.',
        -32601 => 'The method does not exist or is not available.',
        -32602 => 'Invalid method parameter(s).',
        -32603 => 'Internal JSON-RPC error.',
        -32000 => '500',
    ];

    public function __construct($class)
    {
        self::$service = $this;

        $this->callableClass = $class;
        $this->requestUri = \Input::getRequestUri();
    }

    /**
     * Получение тела запроса
     * @return array
     */
    public function getInput()
    {
        return \Input::all() ?? [];
    }

    /**
     * "Перехват" и обработка тела запроса
     * @return array
     */
    public function catchRequest()
    {
        $this->request = $request = $this->getInput();
        /**
         * Очистка запроса от параметра q, в котором передаётся относительный путь запроса (REQUEST_URI)
         * TODO: разобраться в настройке nginx и ситуации с передачей параметра q=... в POST запросе
         */
        if (isset($this->request['q']) && $this->requestUri === '/' . $this->request['q']) {
            unset($this->request['q']);
        }
        if ($this->isBatchRequest()) {
            return $this->handleBatchRequest();
        }
        return $this->handleRequest();
    }

    /**
     * Проверка запроса пачкой
     * @return bool
     */
    public function isBatchRequest()
    {
        if (isset($this->request[0])) {
            return true;
        }
        return false;
    }

    /**
     * Обраюотка запроса пачкой
     * @return array
     */
    public function handleBatchRequest()
    {
        $requests = $this->request;
        $batchResponse = [];
        foreach ($requests as $req) {
            $this->request = $req;
            $this->error = null;
            if ($response = $this->handleRequest()) {
                $batchResponse[] = $response;
            }
        }
        return $batchResponse;
    }

    /**
     * Обработка единиччного запроса
     */
    public function handleRequest()
    {
        if (!$this->validateRequest()) {
            return $this->error;
        }
        if (!$this->validateJsonRpc()) {
            return $this->error;
        }
        $this->jsonrpc = $this->getJsonRpc();
        if (!$this->validateId()) {
            return $this->error;
        }
        $this->id = $this->getId();
        if (!$this->validateMethod()) {
            return $this->error;
        }
        $this->method = $this->getMethod();
        $this->params = $this->getParams();

        // Handle
        try {
            $this->executeMethod();
        } catch (Exception $e) {
            $this->setError(self::ERR_SERVER_500, $e->getMessage());
        }
        //Валидация ответа
        // Ответ
        return $this->setResponse();
    }


    public function validateRequest()
    {
    // Валидация JSON
        if ($this->request === []) {
            return $this->setError(
                self::ERR_INVALID_JSON,
                'Invalid JSON Request formatting'
            );
        }
        if (!is_array($this->request)) {
            return $this->setError(
                self::ERR_INVALID_JSON,
                'Invalid JSON Request formatting'
            ) ?: $this->error;
        } //Валидация обязательных полей
        if (count($this->request) !== 4) {
            return $this->setError(
                self::ERR_INVALID_REQUEST,
                'Invalid count of arguments'
            ) ?: $this->error;
        }
        return true;
    }

    /**
     * Получение обязательного "поля" Id
     * @return null
     */
    public function getId()
    {
        return $this->request['id'] ?? null;
    }

    public function validateId()
    {

        if (!$this->getId()) {
            return $this->setError(
                self::ERR_INVALID_REQUEST,
                'ID parameter is invalid'
            );
        }
        return true;
    }

    protected function getJsonRpc()
    {
        return $this->request['jsonrpc'] ?? null;
    }

    /**
     * Получение обязательного "поля" JsonRpc
     */
    protected function validateJsonRpc()
    {
        if (!$this->getJsonRpc() || $this->getJsonRpc() !== '2.0') {
            return $this->setError(
                self::ERR_INVALID_REQUEST,
                'JsonRpc parameter is invalid'
            );
        }
        return true;
    }

    /**
     * Получение обязательного "поля" method
     * @return null
     */
    public function getMethod()
    {
        return $this->request['method'] ?? null;
    }

    /**
     * Валидация метода
     * @return bool
     */
    protected function validateMethod()
    {
        if (!$this->getMethod() || !is_string($this->getMethod())) {
            return $this->setError(
                self::ERR_INVALID_REQUEST,
                'Method parameter is invalid'
            );
        }
        if (!is_callable([$this->callableClass, $this->getMethod()])) {
            return $this->setError(self::ERR_METHOD_NOT_FOUND);
        }
        return true;
    }

    /**
     * Получение обязательного "поля" params
     * @return null
     */
    protected function getParams()
    {
        return $this->request['params'] ?? null;
    }

    /**
     * Ошибка
     * http://www.jsonrpc.org/specification#error_object
     * @param $error_code10 .02.2017 RpcServer.php
     * file:///C:/ex/classes/RpcServer.php.html 5/5
     * @param string $data
     * @return bool
     */
    protected function setError($error_code, string $data = '')
    {
        $this->error = [
            'jsonrpc' => '2.0',
            'error' => [
                'code' => $error_code,
                'message' => self::$errors[$error_code] ?? 'Undefined error',
                'data' => $data,
            ],
            'id' => $this->getId(),
        ];
        return false;
    }

    protected function executeMethod()
    {
        $class = $this->callableClass;
        $method = $this->method;
        $params = $this->params;
        if (!$this->result = $class::$method($params)) {
            throw new \RuntimeException("Method '" . $method . "' execute error");
        }
        return $this->result;
    }

    /**
     * Нормализация и формирование ответа
     */
    protected function setResponse()
    {
        if ($error = $this->error) {
            return $error;
        }
        return ['jsonrpc' => $this->jsonrpc,
            'result' => $this->result,
            'id' => $this->id,
        ];
    }
}